
  //Clase Local

  class Local extends Inmueble{
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas, nVentanas,persianas,tipo,suelo,extractoraHumo,cafetera,mobiliario,puertoCargaDescarga) {
      super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas);
      this.nVentanas=nVentanas;
      this.persianas=persianas;
      this.tipo=tipo;
      this.suelo=suelo;
      this.extractoraHumo=extractoraHumo;
      this.cafetera=cafetera;
      this.mobiliario=mobiliario;
      this.puertoCargaDescarga=puertoCargaDescarga;
      }
      // Método para calcular el precio de el local
      calcularPrecio() {
        let precio = super.calcularPrecio();
        //suelo urbano 1 suelo industrial 0;
        if(this.suelo==1){
          precio = precio +(precio*0.25);

        }
        if(this.metrosCuadrados>50){
          precio = precio + (precio * 0.01);
        }
        if(this.nVentanas<=1){
          precio = precio - (precio * 0.02);

        }else if(this.nVentanas>4){
          precio = precio + (precio * 0.02);

        }
        
        return precio;
      }    
  }
