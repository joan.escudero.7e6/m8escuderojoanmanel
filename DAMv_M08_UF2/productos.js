
function muestraProductos() {
  const productosDiv = document.getElementById("productos");

  productos.forEach(producto => {
    const article = document.createElement("article");
    article.classList.add("articulo");

    const div = document.createElement("div");

    const nombre = document.createElement("p");
    nombre.classList.add("nombre", "w50");
    nombre.textContent = producto.nombre;
    div.appendChild(nombre);

    if (producto.descuento) {
      const descuento = document.createElement("p");
      descuento.classList.add("descuento");
      descuento.textContent = producto.descuento;
      div.appendChild(descuento);
    }

    article.appendChild(div);

    const img = document.createElement("img");
    img.src = `src/${producto.foto}`;
    article.appendChild(img);

    const precio = document.createElement("p");
    const span = document.createElement("span");
    span.textContent = `${producto.precio}€`;
    precio.appendChild(span);
    article.appendChild(precio);

    const tipoProducto = document.createElement("p");
    tipoProducto.textContent = producto.tipoProducto;
    article.appendChild(tipoProducto);

    const cantidad = document.createElement("p");
    const input = document.createElement("input");
    input.type = "number";
    input.id = producto.nombre;
    input.step = "1";
    input.min = "0";
    cantidad.appendChild(input);
    article.appendChild(cantidad);

    productosDiv.appendChild(article);
  });

  const comprarButton = document.createElement("button");
  comprarButton.textContent = "Añadir productos";
  productosDiv.appendChild(comprarButton);
  comprarButton.addEventListener("click", () => {
    anadirProductos();
    input.textContent="";
  });
}


