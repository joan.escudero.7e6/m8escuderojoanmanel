function recuperaDatosUsuario() {
    var visited = localStorage.getItem("visited");
    if (!visited) {
        //abrimos ventana de configuración
        window.open('form.html', 'Config', 'toolbar=yes,statusbar=yes,width=600,height=400')

        //Especificamos el evento que nos indica que la ventana ha sido cerrada
        window.addEventListener("message", function (event) {
            if (event.data === "ventana_cerrada") {
                changeConfig();
            }
        });

     } else{changeConfig()}
}
    
    function changeConfig() {
        console.log("Entra Config");
        // Aplicamos la configuración guardada en la cookie a la página principal
        
        var cliente = document.getElementById("cliente");
        user=new Usuario( localStorage.getItem("name"), localStorage.getItem("surname"), localStorage.getItem("address"), "Banco");
        const html = user.codigoHTML();
        cliente.innerHTML = html;
    }

function saveConfig() {
	var name = document.getElementById("name").value;
	var surname = document.getElementById("surname").value;
    var address = document.getElementById("address").value;


	localStorage.setItem("name", name);
	localStorage.setItem("surname", surname);
    localStorage.setItem("address", address);

	//Si activamos esta cookie solo entraremos la primera vez.
	localStorage.setItem("visited", true);
	window.close();

}

//Evento que se lanza al cerrar esta ventana y así avisamos al que nos ha abierto.
window.addEventListener("beforeunload", function () {
	window.opener.postMessage("ventana_cerrada", "*");
});
// Event listener para el submit del formulario de configuración
document.getElementById("datos").addEventListener("submit", function (event) {
	event.preventDefault();
	saveConfig();
});