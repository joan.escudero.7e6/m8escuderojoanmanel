
function anadirProductos() {
    const tableBody = document.getElementById("tablebody");
    const subtotal = document.getElementById("subtotal");
  
    productos.forEach(producto => {
      const input = document.getElementById(producto.nombre);
      const cantidad = parseInt(input.value);
  
      if (cantidad > 0) {
        const fila = document.createElement("tr");
  
        const nombreCell = creaCelda(producto.nombre);
        fila.appendChild(nombreCell);
  
        const precioCell = creaCelda(`${producto.precio}€`);
        fila.appendChild(precioCell);
  
        let descuento = 0;
        let precioDescuento = producto.precio;
        if (producto.descuento) {
          descuento = parseInt(producto.descuento.replace("%", ""));
          precioDescuento = producto.precio * (1 - descuento / 100);
        }
  
        const descuentoCell = creaCelda(`${descuento}%`);
        fila.appendChild(descuentoCell);
  
        const cantidadCell = creaCelda(cantidad);
        fila.appendChild(cantidadCell);
  
        const total = precioDescuento * cantidad;
        const totalCell = creaCelda(`${total}€`);
        fila.appendChild(totalCell);
  
        tableBody.appendChild(fila);
  
        input.value = 0;
      }
    });
  
    let subtotales = 0;
    for (let i = 0; i < tableBody.rows.length; i++) {
      const totalCell = tableBody.rows[i];
      subtotales += parseFloat(totalCell.textContent.replace("€", ""));
    }
    subtotales.textContent = subtotales.toFixed(2);
  }
  