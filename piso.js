
// Clase Piso que hereda de Inmueble
class Piso extends Inmueble {
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas,numHabitaciones,numBaños, piso, ascensor, tieneTerraza) {
      super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas);
      this.numBaños=numBaños;
      this.numHabitaciones=numHabitaciones;
      this.piso = piso;
      this.ascensor = ascensor;
      this.tieneTerraza = tieneTerraza;
    }
  
    // Método para calcular el precio del piso
    calcularPrecio() {
      let precio = super.calcularPrecio();
  
      // Si el piso es un tercero o más y tiene ascensor, su precio incrementa un 3%
      if (this.ascensor && this.piso >= 3) {
        precio += precio * 0.03;
      }
  
      // Si el piso tiene terraza, su precio incrementa en 300€ por cada metro cuadrado de terraza.
      if (this.tieneTerraza) {
        precio += 300 * this.metrosCuadrados;
      }
  
      return precio;
    }
  }