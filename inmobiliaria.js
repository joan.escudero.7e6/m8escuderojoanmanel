// Clase Inmueble
class Inmueble {
  constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas) {
    this.direccion = direccion;
    this.metrosCuadrados = metrosCuadrados;
    this.referenciaCatastral = referenciaCatastral;
    this.precioBase = precioBase;
    this.foto = foto;
    this.estado = estado;
    this.coordenadas = coordenadas;
  }

  // Método para calcular el precio del inmueble
  calcularPrecio() {
    let precio = this.precioBase;
    const anosAntiguedad = new Date().getFullYear() - this.estado.anos;

    // Si el inmueble tiene más de 10 años, se reduce el precio un 1% por cada año hasta llegar al límite de 20 años.
    if (anosAntiguedad > 10) {
      const maxDescuento = Math.min(anosAntiguedad - 10, 10);
      const descuento = maxDescuento / 100;
      precio -= precio * descuento;
    }

    return precio;
  }
}


