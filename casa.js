
// Clase Casa que hereda de Inmueble
class Casa extends Inmueble {
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas, tipo, numHabitaciones, numBanos, jardin, piscina) {
      super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas);
      this.tipo = tipo;
      this.numHabitaciones = numHabitaciones;
      this.numBanos = numBanos;
      this.jardin = jardin;
      this.piscina = piscina;
    }
  
    // Método para calcular el precio de la casa
    calcularPrecio() {
      let precio = super.calcularPrecio();
  
      // Si la casa tiene jardín, su precio incrementa en función del tamaño del jardín.
      if (this.jardin) {
        if (this.jardin > 250) {
          precio += precio * 0.09;
        } else if (this.jardin > 100) {
          precio += precio * 0.05;
        }
      }
  
      // Si la casa tiene piscina, su precio incrementa en 10,000€
      if (this.piscina) {
      precio += 10000;
      }
      return precio;
    }
  
    }