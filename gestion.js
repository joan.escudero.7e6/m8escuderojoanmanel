class Gestor {
    constructor() {
      this.listaInmuebles = [];
      this.numPisos = 0;
      this.numCasas = 0;
      this.numLocales = 0;
    }
  
    // Método para añadir un inmueble a la lista de inmuebles
    agregarInmueble(inmueble) {
      this.listaInmuebles.push(inmueble);
  
      if (inmueble instanceof Piso) {
        this.numPisos++;
      } else if (inmueble instanceof Casa) {
        this.numCasas++;
      } else {
        this.numLocales++;
      }

      console.log("añadido correctamente! " + inmueble.direccion)
    }
  
    // Método para eliminar un inmueble de la lista de inmuebles
    eliminarInmueble(inmueble) {
      const index = this.listaInmuebles.indexOf(inmueble);
      if (index > -1) {
        this.listaInmuebles.splice(index, 1);
  
        if (inmueble instanceof Piso) {
          this.numPisos--;
        } else if (inmueble instanceof Casa) {
          this.numCasas--;
        } else {
          this.numLocales--;
        }
      }
    }
  
    // Método para modificar los datos de un inmueble de la lista de inmuebles
    modificarInmueble(inmueble, nuevosDatos) {
      Object.assign(inmueble, nuevosDatos);
    }
  
    // Método para cambiar el estado de un inmueble de "activo" a "inactivo"
    desactivarInmueble(inmueble) {
      inmueble.estado = "inactivo";
    }
  
    // Método para contar el número de pisos, casas y locales en la lista de inmuebles
    listarInmuebles() {
      return {
        numPisos: this.numPisos,
        numCasas: this.numCasas,
        numLocales: this.numLocales,
      };
    }

    
  
  }
  