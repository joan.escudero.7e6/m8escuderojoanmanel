let Inmobiliaria = [];
let Desactivados = [];

class Inmueble {
    direccion;
    m2;
    refCastral;
    precioBase;
    precioFinal;
    foto;
    estado;
    años;
    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años) {
        this.direccion = direccion;
        this.m2 = m2;
        this.refCastral = refCastral;
        this.precioBase = precioBase;
        this.precioFinal = precioFinal;
        this.foto = foto;
        this.estado = estado;
        this.años = años;
    }

    darAlta() {
        Inmobiliaria.push(this);
    }

    darBaja() {
        let num = Inmobiliaria.indexOf({ direccion: this.direccion });
        Inmobiliaria.splice(num, 1)
    }

    modificar(atributo, cambio) {
        this[atributo] = cambio;
        return this[atributo];
    }

    desactivar() {
        Desactivados.push(this)
        this.darBaja();
    }

    calcularPrecio10Años() {
        if (this.años > 10 && this.años < 20) {
            let num = this.años % 10;
            this.precioFinal -= this.precioBase * (num / 100);
        }
        else if (this.años >= 20) {
            this.precioFinal -= this.precioBase * 0.1;
        }
        return this.precioFinal;
    }
}

class Piso extends Inmueble {
    piso;
    terraza;
    ascensor;
    nHabitaciones;
    nBaños;
    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nHabitaciones, nBaños, piso, terraza, ascensor) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años);
        this.piso = piso;
        this.terraza = terraza;
        this.ascensor = ascensor;
        this.nHabitaciones = nHabitaciones;
        this.nBaños = nBaños;
    }

    calcularPrecioA() {
        if (this.ascensor == true && this.piso >= 3) {
            this.precioFinal += this.precioBase * 0.03;
        }
        return this.precioFinal;
    }

}

class Local extends Inmueble {
    nVentanas;
    persianaMetalica;
    ventanas;

    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica, ventanas) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años);
        this.nVentanas = nVentanas;
        this.persianaMetalica = persianaMetalica;
        this.ventanas = ventanas;
    }

    calcularPrecioL() {
        if (this.m2 > 50) {
            this.precioFinal += this.precioBase * 0.01;
        }
        return this.precioFinal;
    }

    calcularPrecioV() {
        if (this.ventanas == true && this.nVentanas <= 1) {
            this.precioFinal -= this.precioBase * 0.02;
        }
        else if (this.ventanas == true && this.nVentanas > 4) {
            this.precioFinal += this.precioBase * 0.02;
        }
        return this.precioFinal;
    }
}

class Casa extends Inmueble {
    tipo;
    jardin;
    metrosJardin;
    piscina;
    nHabitaciones;
    nBaños;

    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nHabitaciones, nBaños, tipo, jardin, metrosJardin, piscina) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años);
        this.tipo = tipo;
        this.jardin = jardin;
        this.piscina = piscina;
        this.metrosJardin = metrosJardin;
        this.nHabitaciones = nHabitaciones;
        this.nBaños = nBaños;
    }

    calcularPrecioJP() {
        if (this.jardin == true) {
            if (this.piscina == true) {
                this.precioFinal += this.precioBase * 0.04;
            } if (this.metrosJardin > 250) {
                this.precioFinal += this.precioBase * 0.09;
            }
            else if (this.metrosJardin > 100) {
                this.precioFinal += this.precioBase * 0.05;
            }

        }
        return this.precioFinal;
    }
}



class Restauracion extends Local {
    cafetera;
    mobiliario;

    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas, cafetera, mobiliario) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas);
        this.cafetera = cafetera;
        this.mobiliario = mobiliario;
    }
}

class Industrial extends Local {
    puestoCargaDescarga;
    nPuerto;
    suelo;

    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas, puestoCargaDescarga, nPuerto, suelo) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas);
        this.puestoCargaDescarga = puestoCargaDescarga;
        this.nPuerto = nPuerto;
        this.suelo = suelo;
    }

    calcularSueloU() {
        if (this.suelo == false) {
            this.precioFinal *= 1.25;
        }
        return this.precioFinal;
    }
}

class Comercial extends Local {
    adaptado;

    constructor(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas,adaptado) {
        super(direccion, m2, refCastral, precioBase, precioFinal, foto, estado, años, nVentanas, persianaMetalica,ventanas);
        this.adaptado = adaptado;
    }
}

