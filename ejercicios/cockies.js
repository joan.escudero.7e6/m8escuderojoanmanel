// // Función para obtener el valor de una cookie por su nombre
// function getCookie(name) {
//     const cookies = document.cookie.split(';');
//     for (let i = 0; i < cookies.length; i++) {
//       const cookie = cookies[i].trim();
//       if (cookie.startsWith(name + '=')) {
//         return cookie.substring(name.length + 1);
//       }
//     }
//     return null;
//   }
  
//   // Función para crear una cookie con un valor y una duración en días
//   function setCookie(name, value, days) {
//     const date = new Date();
//     date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
//     const expires = date.toUTCString();
//     document.cookie = name + '=' + value + '; expires=' + expires + '; path=/';
//   }
  
//   // Función para mostrar el formulario de configuración al entrar por primera vez
//   function showConfig() {
//     const bgColor = getCookie('bgColor');
//     const textColor = getCookie('textColor');
//     if (bgColor === null || textColor === null) {
//       const form = document.createElement('form');
//       form.innerHTML = `
//         <h2>Configuración</h2>
//         <label for="bgColor">Color de fondo:</label>
//         <input type="color" id="bgColor" name="bgColor">
//         <br>
//         <label for="textColor">Color de la fuente:</label>
//         <input type="color" id="textColor" name="textColor">
//         <br>
//         <button type="submit">Guardar</button>
//       `;
//       form.addEventListener('submit', function(event) {
//         event.preventDefault();
//         const bgColor = document.getElementById('bgColor').value;
//         const textColor = document.getElementById('textColor').value;
//         setCookie('bgColor', bgColor, 365);
//         setCookie('textColor', textColor, 365);
//         document.body.style.backgroundColor = bgColor;
//         document.body.style.color = textColor;
//         document.body.removeChild(form);
//       });
//       document.body.appendChild(form);
//     } else {
//       document.body.style.backgroundColor = bgColor;
//       document.body.style.color = textColor;
//     }
//   }
  
//   // Llamamos a la función showConfig cuando se carga la página
//   window.open.addEventListener('load', showConfig);
  

function creaCookies() {
  //Creamos la cookie
  document.cookie = "nombre=Alicia";
  document.cookie = "comidaPreferida=Risotto";
}

function visualizaCookies() {
  //Visualizamos
  //console.log(document.cookie); 

  //Todas las cookies estan sepradas por ; por lo que es interesante crear un array para trabajarlas.
  var misCookies = document.cookie;
  var arrayCookies = misCookies.split(";");

  var comida;
  var nombre;
  arrayCookies.map(
      (cookie) => {
          c = cookie.split("=");
          console.log(c);
          if (c[0].includes("comidaPreferida")) {
              comida = c[1];
          }
          if (c[0] == "nombre") {
              nombre = c[1];
          }
      }
  );
  alert("A " + nombre + " le encanta el" + comida);
}

function eliminarCookies() {
  document.cookie.split(";").forEach(function (c) {
      document.cookie = c + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  });
