// Obtenemos el canvas y su contexto
const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Dibujamos el fondo
ctx.fillStyle = "#C8C8C8";
ctx.fillRect(0, 0, canvas.width, canvas.height);

// Dibujamos la carretera
ctx.fillStyle = "#666666";
ctx.fillRect(canvas.width / 2 - 50, 0, 100, canvas.height);

// Dibujamos las líneas blancas de la carretera
ctx.beginPath();
ctx.strokeStyle = "#FFFFFF";
ctx.lineWidth = 10;
ctx.moveTo(canvas.width / 2 - 5, 0);
ctx.lineTo(canvas.width / 2 - 5, canvas.height);
ctx.moveTo(canvas.width / 2 + 5, 0);
ctx.lineTo(canvas.width / 2 + 5, canvas.height);
ctx.stroke();
