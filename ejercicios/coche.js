  var imagen = document.getElementById("imagen");
  var imagenX = 0;
  var imagenY = 0;

  imagen.onclick = function() {
    var cursorX = event.clientX;
    var cursorY = event.clientY;

    var distanciaX = cursorX - imagenX;
    var distanciaY = cursorY - imagenY;

    var angulo = Math.atan2(distanciaY, distanciaX) * 180 / Math.PI;

    distanciaX -= 50;
    distanciaY -= 50;

    imagen.style.left = cursorX - distanciaX + "px";
    imagen.style.top = cursorY - distanciaY + "px";

    imagen.style.transform = "rotate(" + angulo + "deg)";
  };

  document.onmousemove = function(event) {
    // Actualizar la posición actual de la imagen
    imagenX = imagen.offsetLeft + imagen.offsetWidth / 2;
    imagenY = imagen.offsetTop + imagen.offsetHeight / 2;
  };
